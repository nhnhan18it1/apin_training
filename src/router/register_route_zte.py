from flask import jsonify, request
from flask_cors import cross_origin
from app.db import db
from helper.auth_token import token_require
import pandas as pd

def register_route_zte(app):
    @app.route("/getMOName", methods=["GET"])
    @cross_origin()
    @token_require
    def getMOname(data):
        listMOName = db["ZTE_3G"].distinct("@class")
        return {"msg":"fetch data success","data":listMOName,"user":data},200

    @app.route("/getBaseline", methods=["POST"])
    @cross_origin()
    def getBaseline():
        baselines = request.json["baseline"]
        datetime = request.json["dateTime"]
        output = []
        for baseline in baselines:
            data = list(db["ZTE_3G"].find({"@class":baseline["table_name"], "dateTime": datetime}))
            df = pd.DataFrame(data)
            filterdata = df[df["USERLABEL"].str.contains(baseline["cellname"])]
            filterdata.insert(2, f"baseline_{baseline['baseline_key']}", baseline["baseline_value"])
            baseliners = filterdata[[f"baseline_{baseline['baseline_key']}", baseline['baseline_key']]]
            tmp = baseliners.to_dict(orient="records")[0]
            tmp["networkElement"] = baseline["cellname"]
            tmp["actal_value"] = tmp[baseline['baseline_key']]
            tmp["baseline_value"] = tmp[f"baseline_{baseline['baseline_key']}"]
            tmp["parameter"] = baseline["baseline_key"]
            tmp["table_name"] = baseline["table_name"]
            output.append(tmp)

        return {"mas":"fetch data success!", "data":output}, 200
