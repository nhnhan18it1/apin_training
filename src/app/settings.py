from os import getenv

BE_HOST = getenv("BE_HOST","0.0.0.0")
BE_PORT = int(getenv("BE_PORT",5000))

MONGODB_HOST = getenv("MONGODB_HOST","192.168.1.70")
MONGODB_PORT = getenv("MONGODB_PORT","27017")
MONGODB_USER = getenv("MONGODB_USER","root")
MONGODB_PASS = getenv("MONGODB_PASS","pass12345")
MONGODB_DBNAME = getenv("MONGODB_DBNAME","itomtest")

USER_AUTH_HOST = getenv("USER_AUTH_HOST","http://localhost:8072")