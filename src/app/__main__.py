import app.settings as settings
from router.register_route_zte import register_route_zte
from app import app

if __name__ == "__main__":
    register_route_zte(app)
    app.run(host=settings.BE_HOST,port=settings.BE_PORT)