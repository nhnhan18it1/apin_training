import pymongo
import app.settings as settings

def init_db_connection():
    con_str = (
        f"mongodb://{settings.MONGODB_USER}:{settings.MONGODB_PASS}"
        f"@{settings.MONGODB_HOST}:{settings.MONGODB_PORT}/?authSource=admin"
    )
    mongoClient = pymongo.MongoClient(con_str)
    db = mongoClient[settings.MONGODB_DBNAME]
    return db

db = init_db_connection()