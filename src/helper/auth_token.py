from functools import wraps
from flask import request
import requests
import app.settings as settings

def token_require(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if "Authorization" in request.headers:
            token = request.headers["Authorization"]
        if not token:
            return {
                "msg": "Authentication token missing!",
                "data":None,
                "error":"Unauthorized"
            }, 401
        try:
            data = requests.get(f"{settings.USER_AUTH_HOST}/api/v1/user/info/",
                                headers={"Authorization":token})
            status_code = data.status_code
            data = data.json()
            if data is None and status_code != 200:
                return {
                           "message": "Invalid Authentication token!",
                           "data": None,
                           "error": "Unauthorized"
                       }, 401
        except Exception as e:
            print(e)
            return {
                       "msg": "Something went wrong!",
                       "data": None,
                       "error": str(e)
                   }, 500
        return f(data, *args, **kwargs) #getMOname()
    return decorated